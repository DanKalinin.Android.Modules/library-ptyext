//
// Created by Dan on 13.12.2021.
//

#ifndef LIBRARY_PTYEXT_PTYFS_H
#define LIBRARY_PTYEXT_PTYFS_H

#include "PtyMain.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} PtyFsType;

extern PtyFsType PtyFs;

void PtyFsLoad(JNIEnv *env);
void PtyFsUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_PTYEXT_PTYFS_H
