//
// Created by Dan on 12.12.2021.
//

#include "PtySession.h"

PtySessionType PtySession = {0};

void PtySessionLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/ptyext/PtySession");
    PtySession.class = (*env)->NewGlobalRef(env, class);
    PtySession.object = (*env)->GetFieldID(env, class, "object", "J");
    PtySession.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void PtySessionUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, PtySession.class);
}

JNIEXPORT void JNICALL Java_library_ptyext_PtySession_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, PtySession.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, PtySession.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_ptyext_PtySession_session(JNIEnv *env, jclass class, jstring cwd, jobjectArray argv, jobjectArray env1) {
    jobject this = NULL;
    PTYSession *object = NULL;
    gchar *sdkCwd = GeJNIEnvGetStringUTFChars(env, cwd, NULL);
    g_auto(GStrv) sdkArgv = GeJNIEnvJObjectArrayGStrvTo(env, argv);
    g_auto(GStrv) sdkEnv1 = GeJNIEnvJObjectArrayGStrvTo(env, env1);
    g_autoptr(GError) sdkError = NULL;

    if ((object = pty_session_new(sdkCwd, sdkArgv, sdkEnv1, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        this = (*env)->NewObject(env, class, PtySession.init);
        (*env)->SetLongField(env, this, PtySession.object, GPOINTER_TO_SIZE(object));
    }

    GeJNIEnvReleaseStringUTFChars(env, cwd, sdkCwd);

    return this;
}

JNIEXPORT jboolean JNICALL Java_library_ptyext_PtySession_kill(JNIEnv *env, jobject this) {
    jint ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, PtySession.object);
    g_autoptr(GError) sdkError = NULL;

    if (pty_session_kill(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}
