//
// Created by Dan on 13.12.2021.
//

#include "PtyFs.h"

PtyFsType PtyFs = {0};

void PtyFsLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/ptyext/PtyFs");
    PtyFs.class = (*env)->NewGlobalRef(env, class);
    PtyFs.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void PtyFsUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, PtyFs.class);
}

JNIEXPORT jboolean JNICALL Java_library_ptyext_PtyFs_mountSync(JNIEnv *env, jclass class, jstring source, jstring directory) {
    jboolean ret = JNI_FALSE;
    gchar *sdkSource = GeJNIEnvGetStringUTFChars(env, source, NULL);
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (pty_fs_mount_sync(sdkSource, sdkDirectory, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, source, sdkSource);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);

    return ret;
}
