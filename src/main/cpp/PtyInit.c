//
// Created by Dan on 02.12.2021.
//

#include "PtyInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    pty_init();

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    PtyFsLoad(env);
    PtySessionLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    PtyFsUnload(env);
    PtySessionUnload(env);
}
