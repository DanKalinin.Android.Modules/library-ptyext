//
// Created by Dan on 12.12.2021.
//

#ifndef LIBRARY_PTYEXT_PTYSESSION_H
#define LIBRARY_PTYEXT_PTYSESSION_H

#include "PtyMain.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jmethodID init;
} PtySessionType;

extern PtySessionType PtySession;

void PtySessionLoad(JNIEnv *env);
void PtySessionUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_PTYEXT_PTYSESSION_H
