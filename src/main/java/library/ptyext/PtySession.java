package library.ptyext;

import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class PtySession extends LjlObject implements Closeable {
    private long object;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public static native PtySession session(String cwd, String[] argv, String[] env) throws GeException;

    public native boolean kill() throws GeException;

    public static PtySession session(String applicationId) throws GeException {
        ArrayList<String> argv = new ArrayList<>();
        argv.add("/data/data/" + applicationId + "/files/usr/bin/login");
        argv.add("-login");

        ArrayList<String> env = new ArrayList<>();
        env.add("PREFIX=/data/data/" + applicationId + "/files/usr");
        env.add("HOME=/data/data/" + applicationId + "/files/home");
        env.add("PATH=/data/data/" + applicationId + "/files/usr/bin");

        return PtySession.session("/data/data/" + applicationId + "/files/home", argv.toArray(new String[0]), env.toArray(new String[0]));
    }
}
