package library.ptyext;

import android.os.Handler;
import android.os.Looper;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class PtyFs extends LjlObject {
    public static native boolean mountSync(String source, String directory) throws GeException;

    public interface MountAsyncCallback {
        void callback(GeException exception);
    }

    public static void mountAsync(String source, String directory, MountAsyncCallback callback) {
        new Thread(() -> {
            try {
                mountSync(source, directory);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
